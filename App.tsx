import React, { FunctionComponent } from 'react';
import RootContextProvider from './src';
import NavigationContainer from './src/navigation/NavigationContainer';

const AppContainer: FunctionComponent<Record<string, unknown>> = () => {
  return (
    <RootContextProvider>
      <NavigationContainer />
    </RootContextProvider>
  );
};

export default AppContainer;
