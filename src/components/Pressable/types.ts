import { PressableProps } from 'react-native';

type CustomPressableProps = PressableProps;

export type { CustomPressableProps };
