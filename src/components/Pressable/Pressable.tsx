import React, { FunctionComponent } from 'react';
import { Pressable } from 'react-native';
import { AppTheme } from '../../themes/theme';
import { CustomPressableProps } from './types';

const CustomPressable: FunctionComponent<CustomPressableProps> = (props) => {
  const { children, ...rest } = props;
  return (
    <Pressable
      android_ripple={{ color: AppTheme.colors.shadowColor }}
      {...rest}>
      {children}
    </Pressable>
  );
};

export default CustomPressable;
