import { ApiResponse } from 'apisauce';
import { AxiosRequestConfig } from 'axios';

type ErrorApiResponse<P> = {
  status?: boolean;
  message: string;
  code?: number;
  data: Array<P>;
};

type SuccessApiResponse<T> = {
  status: boolean;
  message: string;
  data: T;
};

type UseLazySuccessApiResponse<T, U = T> = (
  options?: AxiosRequestConfig,
) => Promise<ApiResponse<T, U>>;

type RequestApiResponse<T, U = T> = Promise<ApiResponse<T, U>>;

export type {
  ErrorApiResponse,
  SuccessApiResponse,
  RequestApiResponse,
  UseLazySuccessApiResponse,
};
