/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
import apisauce, {
  ApisauceInstance,
  ApiResponse,
  HEADERS,
  DEFAULT_HEADERS,
  CancelToken,
} from 'apisauce';
import axios, {
  CancelTokenSource,
  AxiosRequestConfig,
  AxiosInstance,
} from 'axios';
import { RequestApiResponse } from './request.types';

class ApiRequest {
  private opts: {
    headers: HEADERS;
    withCredentials: boolean;
  };
  private baseUrl: string;
  private source: CancelTokenSource = CancelToken.source();
  private api: ApisauceInstance;
  private promise: Promise<ApiResponse<any, any>> | null = null;
  private LocalStorage: any;
  private tokenKey: string | null;

  constructor(
    baseUrl: string,
    localStorage: any,
    tokenKey: string | null,
    opts?: Record<any, any>,
  ) {
    this.baseUrl = baseUrl;
    this.tokenKey = tokenKey;
    this.LocalStorage = localStorage;
    this.opts = {
      withCredentials: true,
      headers: DEFAULT_HEADERS,
      ...opts,
    };

    this.api = apisauce.create({
      // base URL is read from the "constructor"
      baseURL: `${this.baseUrl}`,
      // here are some default parameters
      ...this.opts,
    });

    this.addAsyncRequestTransform();
  }

  /**
   * For requests, you are given a request object. Mutate anything in here to change anything about the request.
   */
  private addAsyncRequestTransform() {
    this.api.addAsyncRequestTransform((request) => async () => {
      const token = this.LocalStorage
        ? await this.LocalStorage.getAsyncItem(this.tokenKey)
        : '';

      request.headers['Authorization'] = token ? `Bearer ${token}` : '';
    });
  }

  /**
   * Execute the request.
   * & Set the AUTH token for any request.
   */
  private run<T, U>(call: Promise<ApiResponse<T, U>>) {
    this.promise = call;
  }

  public request = async <T, U = T>(
    url: string,
    params?: AxiosRequestConfig,
  ): RequestApiResponse<T, U> => {
    const CancelToken = axios.CancelToken;
    this.source = CancelToken.source();

    this.run<T, U>(
      this.api.any({
        method: 'GET',
        url,
        cancelToken: this.source.token,
        ...params,
      }),
    );

    const promise = this.getResponse<T, U>();
    const response = await promise;
    return response;
  };

  /**
   * @returns {Promise} Promise for the result of the request.
   */
  private getResponse<T, U>(): Promise<ApiResponse<T, U>> {
    if (!this.promise) {
      throw new Error(
        'ApiRequest#getResponse() called before ApiRequest#request(), so no promise exists yet',
      );
    }

    return this.promise;
  }

  /**
   * Abort the network operation.
   * (the message parameter is optional)
   */
  public abortRequest(message?: string) {
    this.source.cancel(message);
  }

  /**
   * Change request URL.
   */
  public changeRequestURL(url: string): AxiosInstance {
    return this.api.setBaseURL(url);
  }

  /**
   * Set a header for the request
   */
  public setHeader(key: string, value: string): AxiosInstance {
    return this.api.setHeader(key, value);
  }

  /**
   * Set the headers for the request
   */
  public setHeaders(headers: HEADERS): AxiosInstance {
    return this.api.setHeaders(headers);
  }

  /**
   * Get the api instance
   */
  public getRequestApiInstance(): ApisauceInstance {
    return this.api;
  }
}

export default ApiRequest;
