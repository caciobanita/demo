import React, { createContext, useContext, FunctionComponent } from 'react';
import ApiRequest from '../services/api/request.api';

const RestApiContext = createContext<any>(null);

const RestApiContextProvider: FunctionComponent<IRestApiContextProvider> = (
  props,
): JSX.Element => {
  const { children, client } = props;

  return (
    <RestApiContext.Provider value={client}>{children}</RestApiContext.Provider>
  );
};

interface IRestApiContextProvider {
  client: ApiRequest;
}

export default RestApiContextProvider;

export const useRestApiContext = () => useContext<ApiRequest>(RestApiContext);
