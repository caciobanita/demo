import { DefaultTheme } from '@react-navigation/native';
import { AppTheme } from '../themes/theme';

const NavigationTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: AppTheme.colors.brand.primary,
    background: AppTheme.colors.screenBackgroundColor,
  },
};

export { NavigationTheme };
