import React, { FunctionComponent } from 'react';
import { NavigationContainer as Container } from '@react-navigation/native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import RootNavigator from './navigator/Root.navigator';
import { NavigationTheme } from './Navigation.helper';

const NavigationContainer: FunctionComponent<unknown> = (): JSX.Element => {
  return (
    <SafeAreaProvider>
      <Container theme={NavigationTheme}>
        <RootNavigator />
      </Container>
    </SafeAreaProvider>
  );
};

export default NavigationContainer;
