import React, { FunctionComponent } from 'react';
import { createNativeStackNavigator } from 'react-native-screens/native-stack';
import Screens from '../../constants/Screens.constant';
import FeedScreen from '../../modules/Feed/Feed.screen';
import { FeedProps } from '../../modules/Feed/Feed.types';
import HomeScreen from '../../modules/Home/Home.screen';
import { HomeProps } from '../../modules/Home/Home.types';

type RootNavigatorParamList = {
  [Screens.HOME]: HomeProps;
  [Screens.FEED]: FeedProps;
};

const Stack = createNativeStackNavigator<RootNavigatorParamList>();

const RootNavigator: FunctionComponent<unknown> = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerTopInsetEnabled: false,
      }}>
      <Stack.Screen
        name={Screens.HOME}
        component={HomeScreen}
        options={{ title: 'Netbet' }}
      />
      <Stack.Screen
        name={Screens.FEED}
        component={FeedScreen}
        options={{ title: 'Feed' }}
      />
    </Stack.Navigator>
  );
};

export type { RootNavigatorParamList };
export default RootNavigator;
