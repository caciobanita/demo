import React, { FunctionComponent } from 'react';

import { ApiRequestProvider } from './context';
import ApiRequest from './services/api/request.api';

const apiClient = new ApiRequest(
  'https://api.netbet.com/development',
  null,
  null,
);

const RootContextProvider: FunctionComponent<{
  children: React.ReactElement;
}> = (props): JSX.Element => {
  const { children } = props;
  return <ApiRequestProvider client={apiClient}>{children}</ApiRequestProvider>;
};

export { apiClient };
export default RootContextProvider;
