import { AxiosRequestConfig } from 'axios';
import { useCallback } from 'react';
import { useRestApiContext } from '../context';
import { UseLazySuccessApiResponse } from '../services/api/request.types';

const useLazyApiRequest = <T, U = T>(
  url: string,
): UseLazySuccessApiResponse<T, U> => {
  const ClientRequest = useRestApiContext();

  return useCallback(
    (options?: AxiosRequestConfig) => {
      return ClientRequest.request(url, options);
    },
    [ClientRequest, url],
  );
};

export default useLazyApiRequest;
