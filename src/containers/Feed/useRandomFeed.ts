/* eslint-disable @typescript-eslint/no-explicit-any */
import { useCallback, useReducer } from 'react';
import { ACTION_TYPE, API_SOURCES } from '../../constants/Api.constant';
import { useRestApiContext } from '../../context';
import {
  ErrorApiResponse,
  SuccessApiResponse,
} from '../../services/api/request.types';

import {
  FeedDataState,
  CasionResponseType,
  UseRandomFeedProps,
  UseRandomFeedResponseProps,
  FeedDataAction,
  CasinoSourceListTypes,
} from './types';

const initialState = {
  ok: false,
  loading: false,
  errors: null,
  data: [],
};

type Reducer<S, A> = (prevState: S, action: A) => S;
const reducer = <T>(state = initialState, action: FeedDataAction<T>) => {
  const { type, payload } = action;

  switch (type) {
    case ACTION_TYPE.FETCHING:
      return { ...initialState, loading: true };
    case ACTION_TYPE.FETCHED:
      return { ...initialState, ...payload };
    case ACTION_TYPE.FETCH_ERROR:
      return { ...initialState, ...payload };
    default:
      return state;
  }
};

/*
 * This hook takes care of loading the list for randomFeed for a certain website.
 */
const useRandomFeed = (
  props: UseRandomFeedProps,
): UseRandomFeedResponseProps => {
  const {
    queries: { getRandomFeed },
  } = props;

  const [state, dispatch]: [
    FeedDataState<Array<CasinoSourceListTypes>>,
    React.Dispatch<FeedDataAction<Array<CasinoSourceListTypes>>>,
  ] = useReducer<Reducer<any, any>>(reducer, initialState);

  /*
   * Get the rest api client
   */
  const ApiClient = useRestApiContext();

  /*
   * Build the query
   */
  const getRandomFeedData = useCallback(
    async (source: string) => {
      const requestProps = getRandomFeed(source);
      const response = await ApiClient.request<
        SuccessApiResponse<CasionResponseType>,
        ErrorApiResponse<Array<unknown>>
      >(requestProps.path, {
        params: requestProps.params,
      });

      if (!response.ok) {
        throw new Error(response.data?.message);
      }
      return response.data;
    },
    [ApiClient, getRandomFeed],
  );

  const sortFeedData = useCallback(
    (a: CasinoSourceListTypes, b: CasinoSourceListTypes) => {
      const aTitle = a.title.toUpperCase();
      const bTitle = b.title.toUpperCase();
      if (aTitle < bTitle) {
        return -1;
      } else if (aTitle > bTitle) {
        return 1;
      } else {
        return 0;
      }
    },
    [],
  );

  const handleGetRandomFeed = useCallback(async () => {
    dispatch({ type: ACTION_TYPE.FETCHING });
    const createAsyncRequest = API_SOURCES.map(getRandomFeedData);

    try {
      const responses = await Promise.all(createAsyncRequest);

      const feeds: Array<Array<CasinoSourceListTypes>> = [];
      const errors = new Map();

      responses.forEach((response, index) => {
        if (response?.status) {
          feeds.push(response.data.items);
        } else {
          errors.set(API_SOURCES[index], response?.message);
        }
      });

      const listdata = feeds.flat().sort(sortFeedData);
      if (errors.size) {
        dispatch({
          type: ACTION_TYPE.FETCH_ERROR,
          payload: {
            loading: false,
            data: listdata,
            ok: false,
            errors,
          },
        });
      } else {
        dispatch({
          type: ACTION_TYPE.FETCHED,
          payload: {
            loading: false,
            data: listdata,
            ok: true,
          },
        });
      }
    } catch (error) {
      dispatch({
        type: ACTION_TYPE.FETCH_ERROR,
        payload: {
          loading: false,
          data: [],
          ok: false,
          errors: new Map(['app', error.message]),
        },
      });
    }
  }, [getRandomFeedData, sortFeedData]);

  const handleClearFeedData = useCallback(() => {
    dispatch({
      type: ACTION_TYPE.FETCHED,
      payload: {
        loading: false,
        data: [],
        ok: false,
        errors: null,
      },
    });
  }, []);

  return {
    handleGetRandomFeed,
    handleClearFeedData,
    state,
  };
};

export { useRandomFeed };
