import { ACTION_TYPE } from '../../constants/Api.constant';
import { BuildQueryResponseType } from '../types';

type UseRandomFeedProps = {
  queries: { getRandomFeed: (source: string) => BuildQueryResponseType };
};

type UseRandomFeedResponseProps = {
  handleGetRandomFeed: () => void;
  handleClearFeedData: () => void;
  state: FeedDataState<Array<CasinoSourceListTypes>>;
};

type CasinoSourceListTypes = {
  title: string;
  artist: string;
  label: string;
  year: number;
};

type CasionResponseType = {
  items: Array<CasinoSourceListTypes>;
};

interface FeedDataState<T> {
  ok: boolean;
  loading: boolean;
  errors?: Map<string, string | null> | null;
  data: T;
}

interface FeedDataAction<T> {
  type: ACTION_TYPE;
  payload?: FeedDataState<T>;
}

export type {
  UseRandomFeedProps,
  UseRandomFeedResponseProps,
  CasionResponseType,
  FeedDataState,
  FeedDataAction,
  CasinoSourceListTypes,
};
