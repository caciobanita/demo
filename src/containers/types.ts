type BuildQueryResponseType = {
  path: string;
  params?: Record<string, unknown>;
  data?: Record<string, unknown>;
};

export type { BuildQueryResponseType };
