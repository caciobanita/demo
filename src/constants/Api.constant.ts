enum ACTION_TYPE {
  FETCHING = 'rest/fetching',
  FETCHED = 'rest/fetched',
  FETCH_ERROR = 'rest/fetched_error',
}

const API_SOURCES = ['list1', 'list2', 'list3'];

export { ACTION_TYPE, API_SOURCES };
