import { BuildQueryResponseType } from '../../containers/types';

const getRandomFeed = (source: string): BuildQueryResponseType => {
  const path = '/randomFeed';

  const params: Record<string, string | number> = {
    website: 'casino',
    lang: 'eu',
    device: 'desktop',
    source,
  };

  return { path, params };
};

export default {
  queries: {
    getRandomFeed,
  },
};
