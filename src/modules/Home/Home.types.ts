import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from 'react-native-screens/native-stack';
import Screens from '../../constants/Screens.constant';
import { CasinoSourceListTypes } from '../../containers/Feed/types';
import { RootNavigatorParamList } from '../../navigation/navigator/Root.navigator';

type HomeScreenRouteProp = RouteProp<RootNavigatorParamList, Screens.HOME>;

type HomeScreenNavigationProp = NativeStackNavigationProp<
  RootNavigatorParamList,
  Screens.HOME
>;

type HomeScreenProps = {
  route: HomeScreenRouteProp;
  navigation: HomeScreenNavigationProp;
};

type HomeProps = Record<string, unknown>;

type HomeViewProps = {
  feeds: Array<CasinoSourceListTypes>;
  handleViewFeedDetails: (feed: CasinoSourceListTypes) => void;
};

export type {
  HomeScreenRouteProp,
  HomeScreenNavigationProp,
  HomeScreenProps,
  HomeProps,
  HomeViewProps,
};
