import React, { FunctionComponent, useCallback } from 'react';
import { ActivityIndicator, StyleSheet, Text, View } from 'react-native';
import CustomPressable from '../../components/Pressable/Pressable';
import Screens from '../../constants/Screens.constant';
import { useRandomFeed } from '../../containers/Feed/useRandomFeed';
import { AppTheme } from '../../themes/theme';
import RandomFeedOperations from './Home.helper';
import { HomeScreenProps } from './Home.types';
import HomeView from './Home.view';

const styles = StyleSheet.create({
  container: {
    padding: AppTheme.metrics.baseMargin,
    flex: 1,
  },
  buttonContainer: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: AppTheme.colors.brand.primary,
    marginBottom: AppTheme.metrics.baseMargin,
  },
  buttonText: {
    color: AppTheme.colors.text.secondary,
  },
});

const HomeScreen: FunctionComponent<HomeScreenProps> = (props) => {
  const { navigation } = props;
  const randomFeedProps = useRandomFeed({ ...RandomFeedOperations });

  const { handleGetRandomFeed, handleClearFeedData, state } = randomFeedProps;
  const { data, loading } = state;

  const handleViewFeedDetails = useCallback(
    (feed) => {
      navigation.navigate(Screens.FEED, { feed });
    },
    [navigation],
  );

  return (
    <View style={styles.container}>
      <CustomPressable onPress={handleGetRandomFeed}>
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonText}>Get feed data</Text>
        </View>
      </CustomPressable>
      <CustomPressable onPress={handleClearFeedData}>
        <View style={styles.buttonContainer}>
          <Text style={styles.buttonText}>Clear data</Text>
        </View>
      </CustomPressable>
      {loading && (
        <ActivityIndicator
          animating={loading}
          size="large"
          color={AppTheme.colors.brand.primary}
        />
      )}
      <HomeView feeds={data} handleViewFeedDetails={handleViewFeedDetails} />
    </View>
  );
};

export default HomeScreen;
