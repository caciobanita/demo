import React, { FunctionComponent, useCallback } from 'react';
import { FlatList, StyleSheet, Text, View } from 'react-native';
import CustomPressable from '../../components/Pressable/Pressable';
import { FEED_ITEM_HEIGHT } from '../../constants/Common.constant';
import { CasinoSourceListTypes } from '../../containers/Feed/types';
import { AppTheme } from '../../themes/theme';
import { HomeViewProps } from './Home.types';

const styles = StyleSheet.create({
  item: {
    height: FEED_ITEM_HEIGHT,
    backgroundColor: AppTheme.colors.backgroundColor,
    borderBottomColor: AppTheme.colors.brand.primary,
    borderBottomWidth: StyleSheet.hairlineWidth,
    justifyContent: 'center',
  },
});

const HomeView: FunctionComponent<HomeViewProps> = (props) => {
  const { feeds, handleViewFeedDetails } = props;

  const handleNavigate = useCallback(
    (feed) => {
      handleViewFeedDetails(feed);
    },
    [handleViewFeedDetails],
  );

  const renderItemComponent = useCallback(
    ({ item }: { item: CasinoSourceListTypes }) => {
      return (
        <CustomPressable onPress={() => handleNavigate(item)}>
          <View style={styles.item}>
            <Text>{item.title}</Text>
          </View>
        </CustomPressable>
      );
    },
    [handleNavigate],
  );

  const keyExtractor = (item: CasinoSourceListTypes) =>
    `${item.year}_${item.title}`;

  const getItemLayout = useCallback((_, index) => {
    return {
      length: FEED_ITEM_HEIGHT,
      offset: FEED_ITEM_HEIGHT * index,
      index,
    };
  }, []);

  return (
    <FlatList
      data={feeds}
      renderItem={renderItemComponent}
      getItemLayout={getItemLayout}
      keyExtractor={keyExtractor}
    />
  );
};

export default HomeView;
