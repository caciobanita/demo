import React, { FunctionComponent } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { AppTheme } from '../../themes/theme';
import { FeedViewProps } from './Feed.types';

const styles = StyleSheet.create({
  container: {
    padding: AppTheme.metrics.baseMargin,
    flex: 1,
  },
});

const FeedView: FunctionComponent<FeedViewProps> = (props) => {
  const { feed } = props;

  return (
    <View style={styles.container}>
      <Text>{feed.artist}</Text>
      <Text>{feed.label}</Text>
      <Text>{feed.year}</Text>
    </View>
  );
};

export default FeedView;
