import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from 'react-native-screens/native-stack';
import Screens from '../../constants/Screens.constant';
import { CasinoSourceListTypes } from '../../containers/Feed/types';
import { RootNavigatorParamList } from '../../navigation/navigator/Root.navigator';

type FeedScreenRouteProp = RouteProp<RootNavigatorParamList, Screens.FEED>;

type FeedScreenNavigationProp = NativeStackNavigationProp<
  RootNavigatorParamList,
  Screens.FEED
>;

type FeedScreenProps = {
  route: FeedScreenRouteProp;
  navigation: FeedScreenNavigationProp;
};

type FeedProps = { feed: CasinoSourceListTypes };

type FeedViewProps = FeedProps;

export type {
  FeedScreenRouteProp,
  FeedScreenNavigationProp,
  FeedScreenProps,
  FeedProps,
  FeedViewProps,
};
