import React, { FunctionComponent, useEffect } from 'react';
import { FeedScreenProps } from './Feed.types';
import FeedView from './Feed.view';

const FeedScreen: FunctionComponent<FeedScreenProps> = (props) => {
  const { route, navigation } = props;
  const { params } = route;

  console.log('params', params);

  useEffect(() => {
    navigation.setOptions({
      title: params.feed.title ?? '',
    });
  }, [navigation, params.feed.title]);

  return <FeedView feed={params.feed} />;
};

export default FeedScreen;
