import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

const metrics = {
  marginHorizontal: 10,
  marginVertical: 10,
  baseMargin: 16,
  doubleBaseMargin: 32,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
};

const colors = {
  brand: {
    primary: '#333743',
    secondary: '#FFFFFF',
  },
  text: {
    primary: '#8c8c8c',
    secondary: '#FFFFFF',
  },
  screenBackgroundColor: '#FFFFFF',
  backgroundColor: '#F9F9F9',
  shadowColor: '#00000020',
};

export const AppTheme = {
  colors,
  metrics,
};
